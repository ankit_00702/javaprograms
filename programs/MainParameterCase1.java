public class MainParameterCase1
{
    public static void main(String... args)
    {
        System.out.print("String... args");
    }
}
 class MainMethodCases1
{
    static public  void main(String... arg)
    {
         System.out.print("static public  void main(String... arg)");
    }
}

class MainMethodCases2
{
    public static final void main(String... arg)
    {
         System.out.print("public static final void main(String... arg)");
    }
}

class MainMethodCases3
{
    static public  final void main(String... arg)
    {
         System.out.print("static public  final void main(String... arg)");
    }
}

class MainMethodCases4
{
    final  public static void main(String... arg)
    {
         System.out.print("static public  final void main(String... arg)");
    }
}

class MainMethodCases5
{
    final strictfp public static void main(String... arg)
    {
         System.out.print("final strictfp public static void main(String... arg)");
    }
}